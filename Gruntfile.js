'use strict';

module.exports = function(grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Concat
        concat: {
            options: {
                separator: ';'
            },
            frameworks: {
                src: [
                    // jQuery
                    'bower_components/jquery/jquery.js'
                ],
                dest: 'js/dist/frameworks.js'
            },
            vendor: {
                src: [
                    // GreenSock
                    'bower_components/GreenSock-JS/src/uncompressed/TweenMax.js',
                    'bower_components/GreenSock-JS/src/uncompressed/jquery.gsap.js',
                    // fancyBox
                    'bower_components/fancyBox/jquery.fancybox.js',
                    // jPlayer
                    'bower_components/jPlayer/jquery.jplayer/jquery.jplayer.js',
                    // PreloadJS
                    'bower_components/PreloadJS/lib/preloadjs-0.4.0.min.js',
                    //Spin
                    'bower_components/spin.js/dist/spin.js',
                    //Crossroads, Hasher e Signal
                    'bower_components/js-signals/dist/signals.js',
                    'bower_components/hasher/dist/js/hasher.js',
                    'bower_components/crossroads.js/dist/crossroads.js',
                    //Jquery UI
                    'bower_components/jquery.ui/ui/jquery.ui.core.js',
                    'bower_components/jquery.ui/ui/jquery.ui.widget.js',
                    'bower_components/jquery.ui/ui/jquery.ui.mouse.js',
                    'bower_components/jquery.ui/ui/jquery.ui.slider.js',
                    'bower_components/jquery.ui/ui/jquery.ui.tooltip.js',
                    //SWFObject
                    'bower_components/swfobject/swfobject/swfobject.js'

                ],
                dest: 'js/dist/vendor.js'
            },
            components: {
                src: ['js/helpers.js','js/cover.js','js/video.youtube.js','js/tracking.js'],
                dest: 'js/dist/components.js'
            },
            app: {
                src: [
                    'js/fusaosite.db.js',
                    'js/fusaosite.init.js',
                    'js/fusaosite.header.js',
                    'js/fusaosite.home.js',
                    'js/fusaosite.galeria.js',
                    'js/fusaosite.video.js',
                    'js/fusaosite.quiz.js',
                    'js/fusaosite.carregamento.js',
                    'js/fusaosite.navegacao.js',
                    'js/main.js'
                ],
                dest: 'js/dist/app.js'
            },
            dev: {
                options: {
                    separator: ';'
                },
                src: [
                    'js/dist/frameworks.js',
                    'js/dist/vendor.js',
                    'js/dist/components.js',
                    'js/dist/app.js'],
                dest: 'js/output.js'
            }
        },
        // Uglify
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
                properties: true,
                compress: {
                    global_defs: {
                        "DEBUG": false
                    },
                    dead_code: true
                }
            },
            target: {
                files: {
                    'js/output.js': ['js/output.js']
                }
            },
            // Html5 Shiv
            ie: {
                files: {
                    'js/html5shiv.js': ['bower_components/html5shiv/src/html5shiv.js']
                }
            }
        },

        // Compass
        compass: {
            dev: {
                options: {
                    config: 'config.dev.rb'
                }
            },
            prod: {
                options: {
                    config: 'config.rb'
                }
            }
        },
        // Watch
        watch: {
            js: {
                files: [
                    'Gruntfile.js',
                    'js/helpers.js','js/cover.js','js/video.youtube.js','js/tracking.js',
                    'js/fusaosite.db.js',
                    'js/fusaosite.init.js',
                    'js/fusaosite.header.js',
                    'js/fusaosite.home.js',
                    'js/fusaosite.galeria.js',
                    'js/fusaosite.video.js',
                    'js/fusaosite.quiz.js',
                    'js/fusaosite.carregamento.js',
                    'js/fusaosite.navegacao.js',
                    'js/main.js'],
                tasks: ['concat'],
                options: {
                    livereload: true
                }
            }
            ,
            css: {
                files: ['sass/**/*.scss'],
                tasks: ['compass:dev'],
                options: {
                    livereload: true
                }
            },
            others: {
                files: ['**/*.html','img/**/*'],
                options: {
                    livereload: true
                }
            }
        }
    });

    grunt.registerTask('build', ['concat', 'uglify', 'compass:prod']);
    grunt.registerTask('default', ['build']);
};
