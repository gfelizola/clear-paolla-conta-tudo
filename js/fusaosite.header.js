/*===== HEADER =====*/
//Header (não é tratado como uma área =P)
fusaosite.Header = {
    aberto: false,
    entrou: new signals.Signal(),

    entrar: function(nextParams) {
        $('.header').show();

        TweenMax.from( $('.header .titulo'), 1, { css:{ opacity:0 }, delay:1 } );
        TweenMax.from( $('.header .titulo strong'), 0.5, { width:0, delay:2, ease:Quad.easeOut } );
        TweenMax.from( $('.header .titulo strong span'), 1, { opacity:0, delay:2.5 } );

        TweenMax.from( $('.header .logo img'), 0.7, { left:-200, delay:3.2, ease:Quad.easeOut } );
        TweenMax.from( $('.header .logo span'), 0.7, { left:-200, delay:3.5, ease:Quad.easeOut } );

        TweenMax.from( $('.header .extras'), 0.7, { marginRight:-200, delay:3.8, ease:Quad.easeOut } );
        TweenMax.from( $('.header .ico-twitter'), 0.5, { opacity:0, delay:4.2, ease:Quad.easeOut } );
        TweenMax.from( $('.header .ico-facebook'), 0.5, { opacity:0, delay:4.3, ease:Quad.easeOut } );

        if( ! fusaosite.Header.aberto ) fusaosite.Header.configurarEventos();

        TweenMax.delayedCall(3, fusaosite.Header.entrou.dispatch, nextParams);
        fusaosite.Header.aberto = true ;
    },

    configurarEventos: function () {
        $('.header .extras .compartilhe a').click(function(e) {
            e = e || event ;
            e.preventDefault();
            var url = encodeURIComponent(window.location.href);

            if( $(this).hasClass('ico-twitter') ){
                url = "http://www.twitter.com/intent/tweet?url=" + url;
                fusaosite.Navegacao.gaEvento('menu-superior','clique','twitter');
            } else {
                url = "http://www.facebook.com/sharer/sharer.php?u=" + url;
                fusaosite.Navegacao.gaEvento('menu-superior','clique','facebook');
            }

            window.open( url, "compartilhe", "height=300,width=550,resizable=1" );
        });
    }
};
