// Video
(function(window) {
    'use strict';

    var $                 = window.jQuery;
    var videos            = window.videos;
    var urlPath           = window.urlPath;
    var getRandomInt      = window.getRandomInt;
    var $jplayer          = $("#video #jp-player");
    var $jcontrols        = $("#video .jp-controls");
    var jplayerCover      = null;
    var $viewport         = $("#video #viewport-player");
    var jplayerReady      = false;
    var bkpMedia          = null;
    var currentTimeNumber = 0;
    var currentResolution = 'st';
    var currentMedia      = null;
    var currentVideoIndex = '';
    var posicionouCues    = false ;
    var cueDefaultClass   = 'bt-interacao' ;
    var tempoMostrandoCue = 5 ;
    var videoPlayTrack    = $('#videoPer0');
    var modoControleAtual = 'jplayer';
    var videoPer = {
        videoPer25  : $('#videoPer25'),
        videoPer50  : $('#videoPer50'),
        videoPer75  : $('#videoPer75'),
        videoPer100 : $('#videoPer100')
    };

    var jpConfigs = {
        swfPath            : urlPath() + "/swf/",
        solution           : "flash, html",
        supplied           : "ogv, m4v",
        size               : {
            width           : "640px",
            height          : "360px"
        },
        smoothPlayBar      : true,
        keyEnabled         : true,
        autoplay           : true
    }

    jpConfigs.cssSelectorAncestor = ".video-principal";
    jpConfigs.ready = videoReady;
    $jplayer.jPlayer(jpConfigs);

    // Progress Animado
    var progressTween = TweenMax.to(playseek, 10, {
        "backgroundPosition": "-" + 855 + "px 0px",
        "ease"              : SteppedEase.config(855),
        "repeat"            : -1,
        "paused"            : true
    });

    // Swap
    function swapMedia(obj, swapResolution) {
        if( jplayerReady ){
            currentMedia = obj ;
            var iniciarEm = swapResolution ? currentTimeNumber : 0;

            if(iniciarEm == 0) {
                var slugAtual = currentMedia.slug || currentMedia.params[0];
                currentMedia.slug = slugAtual
                currentVideoIndex = slugAtual.substr(slugAtual.length-1);
            }

            if( currentVideoIndex != 4 ){
                modoControleAtual = 'jplayer';
                $jplayer.jPlayer("setMedia", currentMedia.medias[currentResolution]);
                $jplayer.jPlayer("play", iniciarEm );

                console.log('swapMedia', iniciarEm);

                $('#videoYT').hide();
                $('#jp-player').show();
                if( ! swapResolution ) criarCuePoints();
            } else {
                modoControleAtual = 'youtube';
                $('#videoYT').show();
                $('#jp-player').hide();

                window.swapYoutubeVideo("vTifjSfM12M");
            }
        } else {
            bkpMedia = obj;
        }
    }

    function criarCuePoints () {
        posicionouCues = false ;
        $jcontrols.find('.' + cueDefaultClass).remove();

        var pontos = currentMedia.pontos ;
        $.each(pontos, function(index, val) {
            var link = $('<a href="#" class="' + cueDefaultClass + '" data-track="{\'category\':\'video' + currentVideoIndex + '\', \'action\':\'clique\', \'label\':\'interacao' + (index+1) + '\'}">' +
                '<h3 class="titulo">' + val.titulo + '</h3>' +
                '<span class="efeito"></span>' +
                '<span class="imagem"></span>' +
                '<span class="icone"></span>' +
                '<img src="img/interacao/botao-ponto.jpg" alt="" class="cue-point" />' +
                '</a>');

            var urlFinal = val.slug; //.replace('{current-media-slug}', currentMedia.slug);
            link.attr('href','#/videos/' + currentMedia.slug + '/' + urlFinal);
            link.addClass(val.slug);
            link.data('posicao', val.cue);

            $jcontrols.find('.controls .timeline').append(link);
        });
    }

    function posicionaCuePoints () {
        if( ! posicionouCues ){
            $jcontrols.find('.' + cueDefaultClass).each(function(ind, el) {
                var posicao = $(this).data('posicao');
                var duracao = currentMedia.duracao ;
                var leftPos = Math.round( ( 760 * posicao ) / duracao ) - 100;

                $(this).css({left: leftPos});
            });

            posicionouCues = true ;
        }
    }

    function mostrarCuePoints(tempo) {
        $jcontrols.find('.' + cueDefaultClass).each(function(index) {
            var posicao = $(this).data('posicao');
            if( tempo >= posicao && tempo < posicao + tempoMostrandoCue ){
                if( $(this).css('opacity') == 0 ){
                    TweenMax.fromTo( $(this), 0.3, { css:{autoAlpha:0, scale:2}}, { css:{autoAlpha:1, scale:1, transformOrigin:'100px 100px'}, ease:Back.easeOut });
                    TweenMax.from( $(this).find('.cue-point'), 0.5, { css:{height:0}, ease: Back.easeOut, delay: 0.3 });
                    TweenMax.from( $(this).find('.icone'), 0.5, { css:{ opacity:0 }, ease: Back.easeOut, delay:0.7 });
                    TweenMax.from( $(this).find('.titulo'), 0.5, { css:{ left:0, opacity:0 }, ease: Power2.easeOut, delay:0.7 });

                    TweenMax.to( $(this).find('.efeito'), 2.5, { directionalRotation:"120_cw", ease: Back.easeOut, delay:1 });
                }
            } else if( tempo > posicao + tempoMostrandoCue ){
                if( ! $(this).hasClass('saido') ){
                    var posIcone = $(this).position();
                    var posTime = $jcontrols.find('.timeline').offset();

                    $(this).css({left: posTime.left + posIcone.left, top: posIcone.top + 7 }).addClass('saido');
                    $jcontrols.append( $(this) );

                    TweenMax.to( 
                        $(this), 1, 
                        { css:{ left:-30, top: getPosicaoTop($(this).index()), scale:0.4, autoAlpha:1 }, ease:Strong.easeInOut } 
                    );
                    TweenMax.to( $(this).find('.efeito'), 1, {rotation:0, ease: Strong.easeOut });
                }
            } else {
                if( $(this).css('opacity') > 0 && ! $(this).hasClass('saido') ){
                    TweenMax.to( $(this), 0.3, { css:{autoAlpha:0} });
                }
            }
        });
    }

    function getPosicaoTop (indice) {
        var h = Math.min( $(window).height(), 865);
        return (indice * 110) + 20 - h;
    }

    function videoReady() {
        jplayerReady = true;

        // Cover
        jplayerCover = new Cover($viewport, $jplayer.children('img, video, object'));
        jplayerCover.trigger();

        // Cover
        jplayerCoverYT = new Cover($viewport, $viewport.children('#yTplayer'));
        jplayerCoverYT.trigger();

        // Controls
        var play          = $('#jp-play'),
            pause         = $('#jp-pause'),
            hd            = $('#jp-hd'),
            playseek      = $('#playseek'),
            seekPercent   = $('#seekPercent'),
            currentTime   = $('#currentTime'),
            timeline      = $('#timeline'),
            seconds       = 0;

        

        // Progress
        $jplayer.on($.jPlayer.event.progress, function(ev) {
            seekPercent.css({"width": ev.jPlayer.status.seekPercent + '%'});
        });

        // Previne o multiplos triggers
        var oldPercent = 0;

        // Tocando
        $jplayer.on($.jPlayer.event.timeupdate, function(ev) {
            currentTimeNumber = ev.jPlayer.status.currentTime;
            currentMedia.duracao = ev.jPlayer.status.duration;

            var percent = Math.ceil(ev.jPlayer.status.currentPercentAbsolute);

            if (oldPercent != percent) {
                if (videoPer['videoPer' + percent])
                    videoPer['videoPer' + percent].data('track', "{'category':'tempo-video-" + percent + "', 'action':'watched', 'label':'" + currentMedia.slug + "'}");
                    videoPer['videoPer' + percent].trigger('track.video.percent');
                oldPercent = percent;
            }

            if( ! posicionouCues ) posicionaCuePoints();
            mostrarCuePoints(currentTimeNumber);

            jplayerCover.trigger();
            $('.header').addClass('interna');
        });

        // Playa
        $jplayer.on($.jPlayer.event.play, function(e) {
            progressTween.resume();
            jplayerCover.trigger();

            $('.header').addClass('interna');
        });

        $jplayer.on($.jPlayer.event.pause, function(e) {
            progressTween.pause();
        });

        // Termina
        $jplayer.on($.jPlayer.event.ended, function(e) {
            $('.header').removeClass('interna');
            currentTimeNumber = 0;
            progressTween.pause();
            fusaosite.Video.mostrarConteudoFinal();

            play.one('click', function(event) {
                fusaosite.Video.removeConteudoFinal();
                $jplayer.jPlayer('stop');
                $jplayer.jPlayer('play',0);
            });
        });

        //Play
        play.click(function(e) {
            e = e || event; if(e) e.preventDefault();
            console.log('teste play');

            if( modoControleAtual == 'youtube' ){
                window.playVideoYT();
            } else {
                $jplayer.jPlayer('play');
            }
        });

        //Play
        pause.click(function(e) {
            e = e || event; if(e) e.preventDefault();
            console.log('teste pause');

            if( modoControleAtual == 'youtube' ){
                window.pauseVideoYT();
            } else {
                $jplayer.jPlayer('pause');
            }
        });

        //HD
        hd.on('click', function(e) {
            e = e || event;
            if(e) e.preventDefault();

            currentResolution = currentResolution == 'hd' ? 'st' : 'hd';
            $(this).removeClass('video-hd-on').removeClass('video-hd-off');
            $(this).addClass( currentResolution == 'hd' ? 'video-hd-on' : 'video-hd-off' )
            swapMedia(currentMedia, true);
        }).tooltip({ 
            content: "Está lento? Clique e assista em qualidade média"
        });

        if( bkpMedia != null ){
            swapMedia(bkpMedia);
            bkpMedia = null;
        }
    }

    //Manter controles visiveis
    function validaPosicaoSeeker () {
        var $vid = $('#video.area');
        var $wid = $(window);
        var posTotal = $wid.height() + $wid.scrollTop();

        if( $vid.height() > posTotal ){
            $jcontrols.css({top:posTotal - 55});
        } else {
            $jcontrols.css({top:$vid.height() - 55});
        }
    }

    function validaResizeVideo (e) {
        validaPosicaoSeeker();
        $jcontrols.find('.' + cueDefaultClass + '.saido').each(function(index) {
            $(this).css({ top: getPosicaoTop(index+1) });
        });
    }

    $(window).scroll(validaPosicaoSeeker);
    $(window).resize(validaResizeVideo);
    validaPosicaoSeeker();

    //Tamanho do video com efeito Cover
    function Cover(vp, els) {
        var that = this;
        this.$win = $(window);
        this.viewport = vp;
        this.els = [];
        $.each(els, function(i, v){
            that.els.push($(v));
        });

        // Tamanho Original
        this.ow = this.els[0].width();
        this.oh = this.els[0].height();

        // Init
        this.init();
    }

    Cover.prototype.init = function() {
        this.$win
        .on('resize.cover', {"that": this}, this.resize)
        .trigger('resize.cover');
    };

    Cover.prototype.trigger = function() {
        this.$win.trigger('resize.cover');
    };

    Cover.prototype.resize = function(ev) {
        var min_w = 320;
        var that = ev.data.that;
        var $el = that.viewport.parent(),
        $elW = $el.width(),
        $elH = $el.height(),
        ow = that.ow,
        oh = that.oh,
        scale_h = $elW / ow,
        scale_v = $elH / oh,
        scale = scale_h > scale_v ? scale_h : scale_v;

        that.viewport.width($elW);
        that.viewport.height($elH);

        if (scale * ow < min_w) scale = min_w / ow;

        $.each(that.els, function(i, $v){
            $v.width(scale * ow);
            $v.height(scale * oh);
        });

        that.viewport.scrollLeft((that.els[0].width() - $el.width()) / 2);
        that.viewport.scrollTop((that.els[0].height() - $el.height()) / 2);
    };

    window.trocaVideo = swapMedia;
    window.jplayer = $jplayer;
    window.jpConfigs = jpConfigs;
    window.progressTween = progressTween;

})(window);
