//====================
// Youtube Video Manager
// Author: Gustavo Felizola
//====================

/**
 * @constructor
 */
function YoutubeVideoManager(){
    this.version = '0.1';
    this._loading = false;
    this._loadQueue = [];
    this._playerInstances = {};
    this._debugMode = false ;
}

YoutubeVideoManager.prototype = {
    carregaPlayer: function( playerID, extras ){
        if( this._gi(playerID) != null ){
            this._log("Player já instanciado anteriormente");
            return this._gi(playerID);
        } else {
            var newInstancia = this._ci(playerID, extras);
            if( extras.autoControls ){
                newInstancia.criaElementos( $(extras.autoControls) );
            }
            this._loadQueue.push(newInstancia);
            if( ! this._loading ) this._loadNextPlayer();

            return newInstancia;
        }
    },

    validaPlayerPronto: function(playerID) {
        if( this._gi(playerID) == null ){
            this._log('Instancia de player: ' + playerID + ' não encontrada na validação de carregamento' );
        }

        var playerRef = document.getElementById(playerID);
        if( playerRef ){
            this._gi(playerID).ref = playerRef;
            this._log('Player ' + playerID + ' instanciado com sucesso');

            this._playerInitials(this._gi(playerID));
        } else {
            this._log('Objeto API ' + playerID + ' não encontrado');
        }

        if( this._loadQueue.length ){
            this._loadNextPlayer();
        } else {
            this._loading = false;
        }
    },

    carregarVideo: function(playerID, videoID) {
        this._log('Carregar Video:', playerID, videoID);

        var yti = this._gi(playerID);
        yti.currentVideoID = videoID;

        if( yti.valido() ){
            yti.ref.loadVideoById(videoID);
            if( this._debugMode ) yti.ref.mute();
        } else {
            yti.playQuandoPronto = true;
        }
    },

    pausaTodos: function() {
        for( var yt in this._playerInstances ){
            if(this._playerInstances[yt].valido()) this._playerInstances[yt].ref.pauseVideo();
        }
    },

    playTodos: function() {
        for( var yt in this._playerInstances ){
            if(this._playerInstances[yt].valido()) this._playerInstances[yt].ref.playVideo();
        }
    },

    stopTodos: function() {
        for( var yt in this._playerInstances ){
            if(this._playerInstances[yt].valido()) this._playerInstances[yt].ref.stopVideo();
        }
    },

    //"privates"
    _gi: function(pID) { //get instance of player
        return this._playerInstances[pID];
    },
    _ci: function(pID, extras) { // create instance of player
        this._playerInstances[pID] = new YoutubeInstance(pID, extras);
        this._playerInstances[pID].gerenciador = this;
        return this._playerInstances[pID];
    },

    _loadNextPlayer: function() {
        this._loading = true;
        var atual = this._loadQueue.shift();

        var playerID = atual.ID;
        var extras = atual.opcoes;

        var atts = { id: atual.ID };
        var params = { 
            allowScriptAccess: 'always',
            wmode: 'opaque'
        };
        
        var opcoes = 'version=3&modestbranding=1&enablejsapi=1&playerapiid=' + playerID;
        var largura = extras.cover ? '1' : extras.largura ? extras.largura : '100%';
        var altura = extras.cover ? '1' : extras.altura ? extras.altura : '100%';
        swfobject.embedSWF('http://www.youtube.com/apiplayer?' + opcoes, playerID, largura, altura, '9', null, null, params, atts);
    },

    _playerInitials: function(obj) {
        var ref = obj.ref,
            options = obj.opcoes;

        obj.pronto = true;
        obj.criaEventos();

        //opcoes
        if( options.autoplay ){
            if( typeof( options.autoplay ) === "string" ){
                this.carregarVideo( obj.ID, options.autoplay );
            }
        }

        if( options.cover ){
            var $viewport = $(ref).parents('.viewport');
            var $resizes = $viewport.find('img, video, object');
            var cover = new Cover($viewport, $resizes);
        }

        if( typeof(obj.onReady) == "function" ){
            var evento = { current: 0, total: obj.currentVideoDuracao, currentPercentage:0, currentLoaded:0 };
            obj.onReady.apply(obj, [evento]);
        }

        if( obj.playQuandoPronto ){
            this.carregarVideo(obj.ID, obj.currentVideoID);
        }
    },

    _log: function() {
        if( this._debugMode ){
            var str = "";
            for (var i = 0; i < arguments.length; i++) {
               str += arguments[i];
               if( i < arguments.length - 1 ) str += " | ";
            };
            console.log('YoutubeVideoManager:', str);
        }
    }
}

/*===== Each Instance Class =====*/
function YoutubeInstance( playerID, extras ){
    this.ID = playerID;
    this.ref = null;
    this.pronto = false;
    this.opcoes = extras;
    this.intervalo = 0;
    this.currentVideoID = null;
    this.currentVideoDuracao = 0;
    this.playQuandoPronto = false;
    this.controles = null;
    this.gerenciador = null;
    this.progressTween = null;
    this.onUpdate = null;
    this.onEnded = null;
    this.onReady = null;
}

YoutubeInstance.prototype = {
    valido: function() {
        return this.pronto && (this.ref && this.ref.getDuration);
    },
    onMudouEstado: function(novoEstado) {
        if( this.controles ){

            if( novoEstado == 1 ){ //playing
                $(this.controles).find('.video-play').addClass('hidden');
                $(this.controles).find('.video-pause').removeClass('hidden');

                var thisAtual = this;
                this.intervalo = setInterval(function() {
                    thisAtual.onAtualizaInfos.apply(thisAtual);
                }, 250);
                this.onAtualizaInfos();
                this.progressTween.resume();
            } else { //todos os outros

                $(this.controles).find('.video-play').removeClass('hidden');
                $(this.controles).find('.video-pause').addClass('hidden');

                clearInterval(this.intervalo);
                this.progressTween.pause();

                if( novoEstado == 0 ){
                    if( typeof(this.onEnded) == "function" ){
                        var evento = { current: this.currentVideoDuracao, total: this.currentVideoDuracao, currentPercentage:100, currentLoaded:100 };
                        this.onEnded.apply(this, [evento]);
                    }
                }
            }
        }
    },
    onMudouQualidade: function(novaQualidade) {
        var btHd = $(this.controles).find('.bt-hd');
        btHd.removeClass('video-hd-on').removeClass('video-hd-off');

        if( ['hd720','hd1080','highres','large'].indexOf(novaQualidade) >= 0 ){
            btHd.addClass('video-hd-on');
        } else {
            btHd.addClass('video-hd-off');
        }
    },
    onErro: function(erro) {
        this.gerenciador._log(erro);
    },

    onAtualizaInfos: function() {
        if( this.controles && this.valido() ){
            this.currentVideoDuracao = this.ref.getDuration();
            var tempoAtual = this.ref.getCurrentTime();
            var pCarregou = this.ref.getVideoLoadedFraction() * 100;
            var pTocou = Math.ceil((tempoAtual * 100) / this.currentVideoDuracao);

             //interface ajustes
            if( pCarregou > 98 ) pCarregou = 100;
            if( pTocou >= 99 ) pTocou = 100;

            $(this.controles).find('.timers .current-time').html( this.helpers.formataTempo( tempoAtual ) );
            $(this.controles).find('.timers .duration').html( this.helpers.formataTempo( this.currentVideoDuracao ) );
            $(this.controles).find('.timeline .buffer-bar').css('width', pCarregou + '%' );
            $(this.controles).find('.timeline .playhead-bar').css('width', pTocou + '%' );

            if( typeof(this.onUpdate) == "function" ){
                var evento = { current: tempoAtual, total: this.currentVideoDuracao, currentPercentage:pTocou, currentLoaded:pCarregou };
                this.onUpdate.apply(this, [evento]);
            }
        }
    },

    criaEventos: function() {
        if( ! this.valido() ) return false;
        var estadoNome    = this.ID + "_onYoutubeMudouEstado";
        var qualidadeNome = this.ID + "_onYoutubeMudouQualidade";
        var erroNome      = this.ID + "_onYoutubeErro";
        var thisAtual     = this;

        window[estadoNome] = function(novoEstado) {
            if( thisAtual ) thisAtual.onMudouEstado(novoEstado);
        }
        window[qualidadeNome] = function(novaQualidade) {
            if( thisAtual ) thisAtual.onMudouQualidade(novaQualidade);
        }
        window[erroNome] = function(erro) {
            if( thisAtual ) thisAtual.onErro(novoEstado);
        }

        this.ref.addEventListener('onStateChange', estadoNome);
        this.ref.addEventListener('onPlaybackQualityChange', qualidadeNome);
        this.ref.addEventListener('onError', erroNome);
    },

    criaElementos: function(container) {
        var $viewport = $('<div />').attr('id', 'viewport-' + this.ID).addClass('viewport');
        var $containerPlayer = $('<div />').attr('id', 'container-video-' + this.ID);
        var $player = $('<div />').attr('id', this.ID);

        var $controles = $('<div />').attr('id', 'controles-' + this.ID).addClass('controls');
        
        var tplControles = '<div class="container-controls">';
            tplControles +=     '<a href="#play" class="bt-play ir video-play">Play</a>';
            tplControles +=     '<a href="#pause" class="bt-pause ir hidden video-pause">Pause</a>';
            tplControles +=     '<div class="timeline">';
            tplControles +=         '<div class="buffer-bar"></div>';
            tplControles +=         '<div class="playhead-bar video-seek"></div>';
            tplControles +=     '</div>';
            tplControles +=     '<span class="timers">';
            tplControles +=         '<span class="current-time">00:00</span> / <span class="duration">00:00</span>';
            tplControles +=     '</span>';
            tplControles +=     '<a href="#hd" class="bt-hd ir video-hd-off" title="Trocar qualidade">HD</a>';
            tplControles += '</div>';

        $controles.html(tplControles);

        $viewport.append($containerPlayer);
        $containerPlayer.append($player);

        container.append($viewport);
        container.append($controles);

        this.controles = $controles;

        var thisAtual = this;
        $controles.find('.video-play').click(function(ev) {
            ev = ev || event;
            if(ev) ev.preventDefault();
            if(thisAtual.valido()) thisAtual.ref.playVideo();
        });
        $controles.find('.video-pause').click(function(ev) {
            ev = ev || event;
            if(ev) ev.preventDefault();
            if(thisAtual.valido()) thisAtual.ref.pauseVideo();
        });
        $controles.find('.bt-hd').click(function(ev) {
            ev = ev || event;
            if(ev) ev.preventDefault();

            var tamanhos = thisAtual.ref.getAvailableQualityLevels();
            var maior = tamanhos[0];
            var menor = tamanhos[tamanhos.length-1];

            if( $(this).hasClass('video-hd-on') && thisAtual.valido() ){
                thisAtual.ref.setPlaybackQuality(menor);
            } else {
                thisAtual.ref.setPlaybackQuality(maior);
            }
        });
        $controles.find('.timeline').click(function(ev) {
            var offset = $(this).offset();
            var posX = ev.clientX - offset.left;
            var segundos = thisAtual.helpers.regra3( posX, 0, $(this).width(), 0, thisAtual.currentVideoDuracao );

            if( !thisAtual.valido() ) return false;
            if( posX < $controles.find('.buffer-bar').width() ){
                thisAtual.ref.seekTo(segundos, false);
            } else {
                thisAtual.ref.seekTo(segundos, true);
            }
        });

        var tweenBarra = TweenMax.to($controles.find('.playhead-bar'), 10, {
            "backgroundPosition": "-" + 855 + "px 0px",
            "ease"              : SteppedEase.config(855),
            "repeat"            : -1,
            "paused"            : true
        });

        this.progressTween = tweenBarra;
    },

    helpers: {
        formataTempo: function(tempo) {
            var hours = Math.floor(tempo / (60 * 60));
           
            var divisor_for_minutes = tempo % (60 * 60);
            var minutes = Math.floor(divisor_for_minutes / 60);
         
            var divisor_for_seconds = divisor_for_minutes % 60;
            var seconds = Math.ceil(divisor_for_seconds);
           
            var tempo = {
                "h": hours,
                "m": minutes,
                "s": seconds
            };

            var minutos = tempo.m + '';
            var segundos = tempo.s + '';

            if( minutos.length < 2 ) minutos = '0' + minutos;
            if( segundos.length < 2 ) segundos = '0' + segundos;

            return minutos + ':' + segundos;
        },
        regra3: function(value, min1, max1, min2, max2) {
            return min2 + (max2 - min2) * ((value - min1) / (max1 - min1));
        }
    }
}

//instancia
ytmanager = new YoutubeVideoManager();

//Métodos obrigatórios do Youtube
var onYouTubePlayerReady = function(playerID){
    ytmanager.validaPlayerPronto(playerID);
}