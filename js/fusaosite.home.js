/*===== HOME =====*/
fusaosite.Home = {
    area: new fusaosite.Area('home'), //objeto comum de todas as áreas
    produtoAberto: false,
    videoModoAtual: 2,
    produtos: ['cuidado-total', 'pos-alisamento'],
    produtoAbriu: new signals.Signal(),
    produtoFechou: new signals.Signal(),
    framesTl: null,
    timer:null,

    entrar: function(params) {
        var sub = params !== undefined ? params[0] : null;
        var produtos = fusaosite.Home.produtos;

        // Se a tela já estiver aberta, somente valida se é pra abrir um produto ou um destaque de vídeo e realiza a ação
        if( fusaosite.Home.area.aberto ){
            if( sub != null ){ //Validando animal que tenta trocar a url na mão
                if( $.inArray(sub, produtos) >= 0 ){
                    fusaosite.Home.mostrarProduto(sub);
                } else {
                    fusaosite.Home.fecharProdutos();
                }
            } else {
                fusaosite.Home.fecharProdutos();
            }
        } else {
            // Else, se for o primeiro carregamento, faz as tratativas e anima.
            var videoModo = sub || fusaosite.Home.videoModoAtual;
            if( videoModo !== undefined ){
                if( $.inArray(sub, produtos) >= 0 ){
                    //Se o link inicial for para um produto, atrasa a abertura para depois da animação
                    TweenMax.delayedCall(4, fusaosite.Home.mostrarProduto, [sub]);
                } else{
                    //Se o link inicial for para algum destaque de video, valida para já abrir no correto
                    if( typeof(videoModo) === "string" && isNaN(videoModo) ){
                        videoModo = Number( videoModo.substring( videoModo.length - 1 ) );
                    }
                    fusaosite.Home.videoModoAtual = videoModo;
                }
            }

            fusaosite.Home.animarEntrada();
        }
    },
    sair: function(params) {
        //TODO
        fusaosite.Home.area.aberto = false ;

        var $home = $('#home.area');
        var tl = new TimelineMax();
        tl.to( $home, 1, { css:{ opacity:0 }});

        fusaosite.Home.area.terminouSaida.dispatch(params);
    },
    configurarEventos: function() {
        // if( fusaosite.Home.eventosConfigurados ) return false;

        var $home = $('#home.area');

        function destaquesOver (e) {
            e = e || event;
            if(e) e.stopPropagation();

            $home.find('.video').off( 'mouseover', destaquesOver);

            var $self   = $(this);
            var atual   = fusaosite.Home.videoModoAtual ;
            var proximo = $self.index() + 1;
            var $v1 = $home.find('.videos .video-1');
            var $v2 = $home.find('.videos .video-2');
            var $v3 = $home.find('.videos .video-3');

            if( atual != proximo ){
                window.stopWatch(true);

                TweenMax.killTweensOf( $v1 );
                TweenMax.killTweensOf( $v2 );
                TweenMax.killTweensOf( $v3 );

                var v1Props = {};
                var v2Props = {};
                var v3Props = {};

                if( proximo == 1 ){
                    v1Props = { left:0, width:500, zIndex:2 };
                    v2Props = { left:500, width:170, zIndex:3 };
                    v3Props = { left:1000-170, width:170, zIndex:3 };
                } else if( proximo == 2 ){
                    v1Props = { left:0, width:170, zIndex:3 };
                    v2Props = { left:0, width:1000, zIndex:2 };
                    v3Props = { left:1000-170, width:170, zIndex:3 };
                } else if( proximo == 3 ){
                    v1Props = { left:0, width:170, zIndex:3 };
                    v2Props = { left:340, width:170, zIndex:3 };
                    v3Props = { left:500, width:500, zIndex:2 };
                }

                if( proximo != 1 ){
                    TweenMax.to( $v1.find('.play'),             0.5, { css:{autoAlpha:0},               ease:Circ.easeInOut, delay:0.0 });
                    TweenMax.to( $v1.find('.fundo.aberto'),     0.5, { css:{autoAlpha:0},               ease:Circ.easeInOut, delay:0.3 });
                    TweenMax.to( $v1.find('.fundo.fechado'),    0.5, { css:{autoAlpha:1},               ease:Circ.easeInOut, delay:0.8 });
                    TweenMax.to( $v1.find('.aba'),              0.5, { css:{height:400, autoAlpha:1},   ease:Circ.easeInOut, delay:1.5 });
                }

                if( proximo != 2 ){
                    $v2.find('.fundo.fechado').removeClass('direita').removeClass('esquerda');
                    $v2.find('.aba').removeClass('direita').removeClass('esquerda');
                    $v2.find('.fundo.fechado').addClass( proximo == 1 ? 'direita' : 'esquerda' );
                    $v2.find('.aba').addClass( proximo == 1 ? 'direita' : 'esquerda' );

                    TweenMax.to( $v2.find('.play'),             0.5, { css:{autoAlpha:0},               ease:Circ.easeInOut, delay:0.1 });
                    TweenMax.to( $v2.find('.fundo.aberto'),     0.5, { css:{autoAlpha:0},               ease:Circ.easeInOut, delay:0.3 });
                    TweenMax.to( $v2.find('.fundo.fechado'),    0.5, { css:{autoAlpha:1},               ease:Circ.easeInOut, delay:0.8 });
                    TweenMax.to( $v2.find('.aba'),              0.5, { css:{height:400, autoAlpha:1},   ease:Circ.easeInOut, delay:1.5 });
                }

                if( proximo != 3 ){
                    TweenMax.to( $v3.find('.play'),             0.5, { css:{autoAlpha:0},               ease:Circ.easeInOut, delay:0.1 });
                    TweenMax.to( $v3.find('.fundo.aberto'),     0.5, { css:{autoAlpha:0},               ease:Circ.easeInOut, delay:0.3 });
                    TweenMax.to( $v3.find('.fundo.fechado'),    0.5, { css:{autoAlpha:1},               ease:Circ.easeInOut, delay:0.8 });
                    TweenMax.to( $v3.find('.aba'),              0.5, { css:{height:400, autoAlpha:1},   ease:Circ.easeInOut, delay:1.3 });
                }

                TweenMax.to( $self.find('.aba'),            0.5, { css:{height:0, autoAlpha:0},     ease:Circ.easeInOut, delay:0 });
                TweenMax.to( $self.find('.fundo.fechado'),  0.5, { css:{autoAlpha:0},               ease:Circ.easeInOut, delay:0 });

                TweenMax.to( $v1,                           0.5, { css:v1Props,                     ease:Circ.easeInOut, delay:0.4 });
                TweenMax.to( $v2,                           0.5, { css:v2Props,                     ease:Circ.easeInOut, delay:0.4 });
                TweenMax.to( $v3,                           0.5, { css:v3Props,                     ease:Circ.easeInOut, delay:0.4 });

                TweenMax.to( $self.find('.fundo.aberto'),   0.5, { css:{autoAlpha:1},               ease:Circ.easeInOut, delay:0.8 });
                TweenMax.to( $self.find('.play'),           0.5, { css:{autoAlpha:1},               ease:Circ.easeInOut, delay:1.2 });

                $self.find('.fundo.aberto img').css({opacity:0}).first().css({opacity:1});

                $home.find('.videos img.titulo-video:not(.titulo-video-' + proximo + ')').stop().fadeOut('fast',function() {
                    $home.find('.videos .titulo-video.titulo-video-' + proximo + '').stop().delay(1000).fadeIn('slow');
                });

                fusaosite.Home.videoModoAtual = proximo;
                TweenMax.delayedCall( 2.5, function() {
                    $home.find('.video').on( 'mouseover', destaquesOver);
                    window.drawTimer(0);
                    fusaosite.Home.tocarFrames();
                });
            } else {
                $home.find('.video').on( 'mouseover', destaquesOver);
            }
        }

        //click para troca do destaque de vídeo
        $home.find('.video').on( 'mouseover', destaquesOver)
             .find('.play' ).on( 'click',     function() {

            var end = window.videos[ $(this).index('#home.area .videos .play') ].slug;
            fusaosite.Navegacao.gaEvento('destaque-home','clique','video-' + end);
            hasher.setHash('videos/' + end );
        });

        //over dos botões de + dos produtos
        $home.find('.produtos .link-conheca, .produtos .produto').hover(function() {
            if( $(this).hasClass('branco') ){
                TweenMax.to( $home.find('.produtos .produto.bege'), 0.5, { css:{ opacity:0.2 } });
                TweenMax.staggerTo( $home.find('.produtos .produto.branco'), 0.3, { scale:1.2 }, 0.1);
            } else {
                TweenMax.to( $home.find('.produtos .produto.branco'), 0.5, { css:{ opacity:0.2 } });
                TweenMax.staggerTo( $home.find('.produtos .produto.bege'), 0.3, { scale:1.2 }, 0.1);
            }
        }, function() {
            TweenMax.to( $home.find('.produtos .produto'), 0.5, { css:{ opacity:1, scale:1 } });
        });

        $home.find('.produtos .produto').click(function(e) {
            var url = '';

            if( $(this).hasClass('branco') ){
                url = $home.find('.produtos .link-conheca.branco').attr('href');
            } else {
                url = $home.find('.produtos .link-conheca.bege').attr('href');
            }

            if( url.indexOf('#') >= 0 ){ url = url.substr(1); }
            if( url.indexOf('/') == 0 ){ url = url.substr(1); }

            hasher.setHash(url);
        });

        //over dos thumbnails de video que levam para galeria
        $home.find('.curiosidades .thumb-video').hover(function() {
            TweenMax.to( $home.find('.curiosidades .thumb-video:not(:eq('+ $(this).index() +'))'), 0.5, { css:{ opacity:0.3 }, ease:Back.easeOut });
            // TweenMax.to( $(this), 0.5, { scale:1.2, ease:Back.easeOut });
            TweenMax.to( $(this).find('span'), 0.5, { opacity:0, delay:0.2, ease:Back.easeOut });
        }, function() {
            // TweenMax.to( $(this), 0.5, { scale:1, ease:Back.easeOut });
            TweenMax.to( $(this).find('span'), 0.5, { opacity:1, ease:Back.easeOut });
            TweenMax.to( $home.find('.curiosidades .thumb-video:not(:eq('+ $(this).index() +'))'), 0.5, { css:{ opacity:1 }, ease:Back.easeOut });
        });
    },
    animarEntrada: function() {
        var self = fusaosite.Home ;
        self.area.comecouEntrada.dispatch();

        var $home = $('#home.area').show();
        var atual = fusaosite.Home.videoModoAtual ;

        $home.find('.video').unbind( 'mouseover');

        var tl = new TimelineMax({onComplete:function() {
            self.area.aberto = true ;
            self.configurarEventos();
            self.tocarFrames();
            self.area.terminouEntrada.dispatch();
        }});

        tl
        .fromTo( $home, 0.5, { css:{opacity:0}}, { css:{opacity:1}}, 0)
        .from( $home.find('.outros-conteudos .produtos'),                               0.5, { css:{opacity:0}, ease:Circ.easeInOut }, 2)
        .from( $home.find('.outros-conteudos .produtos .cond-branco'),                  0.5, { css:{opacity:0, left:'-=50'}, ease:Circ.easeOut }, 2.2)
        .from( $home.find('.outros-conteudos .produtos .xampu-branco'),                 0.5, { css:{opacity:0, left:'-=30'}, ease:Circ.easeOut }, 2)
        .from( $home.find('.outros-conteudos .produtos .xampu-bege'),                   0.5, { css:{opacity:0, left:'+=30'}, ease:Circ.easeOut }, 2.1)
        .from( $home.find('.outros-conteudos .produtos .cond-bege'),                    0.5, { css:{opacity:0, left:'+=50'}, ease:Circ.easeOut }, 2.3)
        .from( $home.find('.outros-conteudos .produtos .link-conheca.branco'),          0.5, { css:{left:'-=50', opacity:0}, ease:Circ.easeOut }, 2)
        .from( $home.find('.outros-conteudos .produtos .link-conheca.bege'),            0.5, { css:{right:'-=50', opacity:0}, ease:Circ.easeOut }, 2.1)
        .staggerFrom( $home.find('.outros-conteudos .produtos .folha'),                 0.5, { css:{right:'+=130', top: '+=80', width:0, height:0, opacity:0}, ease:Circ.easeOut }, 0.2, 2.5)
        .staggerFrom( $home.find('.outros-conteudos .curiosidades .thumb-video'),       0.5, { css:{opacity:0}, ease:Circ.easeOut }, 0.1, 3)
        .staggerFrom( $home.find('.outros-conteudos .curiosidades .thumb-video span'),  0.5, { css:{opacity:0, zoom:2, marginTop:'-=15', marginLeft:'-=15'}, ease:Circ.easeOut }, 0.1, 3.2)

        .from( $home.find('.videos .video-2'),                       0.5, { css:{opacity:0} }, 0.3)
        .to( $home.find('.videos .video-2 .fundo.aberto img:eq(0)'),        0.5, { css:{opacity:1} }, 0.3)
        .from( $home.find('.videos .video-1'),                       0.5, { css:{opacity:0} }, 0.8)
        .from( $home.find('.videos .video-3'),                       0.5, { css:{opacity:0} }, 1)

        .from( $home.find('.videos .video-1 .aba'),                0.5, { css:{height:0}, ease:Circ.easeInOut }, 1.6)
        .from( $home.find('.videos .video-1 .aba p'),              0.5, { css:{right:-160}, ease:Circ.easeInOut }, 2)

        .from( $home.find('.videos .video-3 .aba'),                0.5, { css:{height:0}, ease:Circ.easeInOut }, 1.6)
        .from( $home.find('.videos .video-3 .aba p'),              0.5, { css:{left:-160}, ease:Circ.easeInOut }, 2.1);

        $home.find('.videos .video-' + atual + ' .titulo-video').delay(2500).fadeIn();
    },
    tocarFrames: function() {
        var $home = $('#home.area');
        var fundo = $home.find('.videos .video-' + fusaosite.Home.videoModoAtual + ' .fundo.aberto');
        var frames = fundo.find('img');

        var totalFrames = 5;
        var duracaoFrame = 5;

        TweenMax.killTweensOf(frames);
        TweenMax.killTweensOf(fusaosite.Home.tocarFrames);

        var tl = new TimelineMax();

        tl.to( frames.eq(0), 0.5, { css:{ opacity:0, zIndex:2 }}, 2 );
        tl.to( frames.eq(1), 0.5, { css:{ opacity:1, zIndex:3 }}, 2 );

        tl.to( frames.eq(1), 0.5, { css:{ opacity:0, zIndex:2 }}, 5 );
        tl.to( frames.eq(0), 0.5, { css:{ opacity:1, zIndex:3 }}, 5 );

        fusaosite.Home.framesTl = tl;

        fusaosite.Home.tocarTimer(6.5);
        TweenMax.delayedCall(6.5, fusaosite.Home.tocarFrames);
    },
    tocarTimer: function(duracao) {
        var timer, timerFinish, timerSeconds;
        var atual = fusaosite.Home.videoModoAtual ;
        var $home = $('#home.area');
        var $timer = $home.find('.videos .video-' + atual + ' .timer');

        function drawTimer(percent){
            // $timer.html('<div class="slice'+(percent > 50?' gt50"':'"')+'><div class="pie"></div>'+(percent > 50?'<div class="pie fill"></div>':'')+'</div>');
            if( percent > 50 ){
                $timer.find('.slice').addClass('gt50');
            } else {
                $timer.find('.slice').removeClass('gt50');
            }
            var deg = 360/100*percent;
            $timer.find('.slice .pie').css({
                '-moz-transform':'rotate('+deg+'deg)',
                '-webkit-transform':'rotate('+deg+'deg)',
                '-o-transform':'rotate('+deg+'deg)',
                'transform':'rotate('+deg+'deg)'
            });
        }
        function stopWatch(forceStop){
            var seconds = (timerFinish-(new Date().getTime()))/1000;
            var timer = fusaosite.Home.timer;
            if(seconds <= 0 || forceStop){
                drawTimer(100);
                clearInterval(timer);
            }else{
                var percent = 100-((seconds/timerSeconds)*100);
                drawTimer(percent);
            }
        }
        window.stopWatch = stopWatch;
        window.drawTimer = drawTimer;

        timerSeconds = duracao || 5;
        timerFinish = new Date().getTime()+(timerSeconds*1000);
        fusaosite.Home.timer = setInterval('stopWatch()',50);
    },
    pararFrames: function() {
        window.stopWatch(true);

        TweenMax.killTweensOf(fusaosite.Home.tocarFrames);
        clearInterval(fusaosite.Home.timer);
        fusaosite.Home.framesTl.pause();
    },
    mostrarProduto: function(qual) {
        var $home = $('#home.area');

        //se tiver um bubble de produto aberto, primeiro tem que fechar
        if( fusaosite.Home.produtoAberto ){
            fusaosite.Home.produtoFechou.add( fusaosite.Home.mostrarProduto );
            fusaosite.Home.fecharProdutos(qual);
        } else {
            //e aqui abre o bubble de produto
            fusaosite.Home.produtoAberto = true ;
            fusaosite.Home.produtoFechou.removeAll();
            var produto = $home.find('.produtos .conteudo-produtos.' + qual).show();

            $home.find('.produtos').addClass('aberto');
            $home.find('.layer-produtos').stop().fadeIn().bind('mouseover',function(e) {
                e = e || event;
                e.stopPropagation();
            });

            TweenMax.from( produto, 0.4, { css:{ scale:0, opacity:0, transformOrigin:"left bottom" }, ease:Cubic.easeOut });
            TweenMax.from( produto.find('.conteudo h3'), 0.5, { css:{ opacity:0, paddingTop: 50 }, delay:0.5, ease:Cubic.easeOut });
            TweenMax.from( produto.find('.conteudo p'), 0.5, { css:{ opacity:0, paddingTop: 50 }, delay:0.7, ease:Cubic.easeOut });
            TweenMax.from( produto.find('.fechar'), 0.5, { css:{ opacity:0, scale:2 }, delay:1 });
        }
    },
    fecharProdutos: function(proximo) {
        var selector = '#home.area .produtos .conteudo-produtos';
        //valida se tem que não animar um dos bubbles, para não conflitar.
        if( proximo ) selector += ':not(.' + proximo + ')';

        $('#home .layer-produtos').unbind('mouseover').stop().fadeOut();
        $('#home .produtos').removeClass('aberto');

        $(selector).fadeOut(400, function() {
            fusaosite.Home.produtoAberto = false ;
            fusaosite.Home.produtoFechou.dispatch(proximo);
        });
    }
}