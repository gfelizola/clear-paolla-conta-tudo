// var mediaServer = "http://s3-sa-east-1.amazonaws.com/clear-paollacontatudo";
// var mediaServer = "http://stage.paollacontatudo.clearanticaspa.com.br/videos/"; 
var mediaServer = urlPath();

var videos = [{
    titulo: 'Posso usar meu anticaspa todos os dias?',
    slug: 'posso-usar-meu-anticaspa-todos-os-dias-1',
    youtubeID: 'ejmqgDngGJ8',
    medias: {
        st:{
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO2.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO2.ogv"
        },
        hd: {
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO2_hd.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO2_hd.ogv"
        }
    },
    pontos: [{
        cue: 42,
        titulo: 'Você sabia que a caspa precisa de cuidados diários',
        slug: 'cuidados-diarios',
        youtubeID: 'dsa7Smi2pX8',
        video: {
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO2_esp.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO2_esp.ogv"
        }
    }, {
        cue: 50,
        titulo: 'Wikiclear - Dúvidas sobre caspa?', 
        slug: 'wikiclear',
        youtubeID: 'lMfyiEySJlg',
        video: {
            m4v: mediaServer + "/videos/wikiclear.m4v",
            ogv: mediaServer + "/videos/wikiclear.ogv"
        }
    }]
}, {
    titulo: 'Cabelos sem caspa e hidratados, é possível?',
    slug: 'cabelos-sem-caspa-e-hidratados-e-possivel-2',
    youtubeID: 'A9Brk5Q9yEM',
    medias: {
        st:{
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO1.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO1.ogv"
        },
        hd: {
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO1_hd.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO1_hd.ogv"
        }
    },
    pontos: [{
        cue: 26,
        titulo: 'Conheça o novo Clear Fusão Herbal',
        slug: 'conheca-o-novo-clear-fusao-herbal',
        youtubeID: 'jEKSd9H59x4',
        video: {
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO1_esp.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO1_esp.ogv"
        }
    }, {
        cue: 46,
        titulo: 'Infrográfico – Quer saber mais? Faça o teste.',
        slug: 'quiz'
    }, {
        cue: 52,
        titulo: 'Wikiclear - Dúvidas sobre caspa?',
        slug: 'wikiclear',
        youtubeID: 'lMfyiEySJlg',
        video: {
            m4v: mediaServer + "/videos/wikiclear.m4v",
            ogv: mediaServer + "/videos/wikiclear.ogv"
        }
    }]
}, {
    titulo: 'Alisamento combina com Anticaspa?',
    slug: 'alisamento-combina-com-anticaspa-3',
    youtubeID: 'EELyJuBWOBU',
    medias: {
        st:{
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO3.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO3.ogv"
        },
        hd: {
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO3_hd.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO3_hd.ogv"
        }
    },
    pontos: [{
        cue: 26,
        titulo: 'Como funciona o Clear Fusão Herbal Pós-química?',
        slug: 'como-funciona-clear-fusao-herbal-pos-quimica',
        youtubeID: 'QW868QpYwDA',
        video: {
            m4v: mediaServer + "/videos/CLEAR_WEB_VIDEO3_esp.m4v",
            ogv: mediaServer + "/videos/CLEAR_WEB_VIDEO3_esp.ogv"
        }
    }, {
        cue: 50,
        titulo: 'Wikiclear - Dúvidas sobre caspa?',
        slug: 'wikiclear',
        youtubeID: 'lMfyiEySJlg',
        video: {
            m4v: mediaServer + "/videos/wikiclear.m4v",
            ogv: mediaServer + "/videos/wikiclear.ogv"
        }
    }]
}, {
    titulo: 'Comercial',
    slug: 'comercial-4',
    youtubeID: 'vTifjSfM12M',
    medias: {
        st:{
            m4v: mediaServer + "/videos/comercial_borboleta.m4v",
            ogv: mediaServer + "/videos/comercial_borboleta.ogv"
        },
        hd: {
            m4v: mediaServer + "/videos/comercial_borboleta_hd.m4v",
            ogv: mediaServer + "/videos/comercial_borboleta_hd.ogv"
        }
    },
    pontos: []
}, {
    titulo: 'Making Of',
    slug: 'making-of-5',
    youtubeID: 'BODXk6Gt2M8',
    medias: {
        st:{
            m4v: mediaServer + "/videos/making-of.m4v",
            ogv: mediaServer + "/videos/making-of.ogv"
        },
        hd: {
            m4v: mediaServer + "/videos/making-of_hd.m4v",
            ogv: mediaServer + "/videos/making-of_hd.ogv"
        }
    },
    pontos: []
}];

