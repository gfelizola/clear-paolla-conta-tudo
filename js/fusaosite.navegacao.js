/*===== NAVEGACAO =====*/
fusaosite.Navegacao = {
    rotas:[],
    atual:null,
    inicial: 'inicio',
    ultimaHash: '',

    //metodo chamado a cada vez que a rota muda.
    navega: function(rota, data) {
        // console.log('Navegacao' , '#', rota, data);

        var atual, rotas = fusaosite.Navegacao.rotas;
        switch(data.route){
            case rotas['home']      : atual = fusaosite.Home;     break;
            case rotas['galeria']   : atual = fusaosite.Galeria;  break;
            case rotas['video']     : atual = fusaosite.Video;    break;
        }

        //se o Header ainda não animou, anima ele primeiro e depois a tela ...
        if( ! fusaosite.Header.aberto ){
            fusaosite.Header.entrou.addOnce( fusaosite.Navegacao.navega );
            fusaosite.Header.entrar([rota, data]);
        } else {
            // ... caso contrário, tira tela atual e anima a entrada da proxima tela

            // antes de abrir qualquer tela, verifica se existe algum player de video, e se tiver, pausa o video
            if( fusaosite.Video.area.id != atual.area.id ){
                //se a area que estiver aberta for a de video, manda alguns parametros extras.
                // abrirImediatamente - é para que a galeria não tenha delay para entrar.
                if( fusaosite.Navegacao.atual.area.id == fusaosite.Video.area.id ){
                    data.params.abrirImediatamente = true ;
                    if( window.jplayer ) window.jplayer.jPlayer('pause');
                }
            }

            // verifica se existe alguma tela aberta, e se essa não é a galeria
            if( fusaosite.Navegacao.atual && atual.area.id != fusaosite.Galeria.area.id ){
                // verifica se a  área atual já esta aberta
                if( fusaosite.Navegacao.atual.area.id == atual.area.id ){
                    atual.entrar(data.params); //so chama isso para alguns tratamentos como troca de video
                } else {
                    // se nao estiver, sai com a tela atual
                    fusaosite.Navegacao.atual.area.terminouSaida.addOnce( atual.entrar );
                    fusaosite.Navegacao.atual.sair(data.params);
                }
            } else {

                // se a area que vai abrir for a galeria, valida o link do fechar para home ou video
                if( atual.area.id == fusaosite.Galeria.area.id ){
                    if( fusaosite.Navegacao.atual.area.id == fusaosite.Video.area.id ){
                        $('#gallery .geral-fechar').attr('href','#/videos/' + window.videos[ fusaosite.Video.videoAtual ].slug );
                    } else {
                        $('#gallery .geral-fechar').attr('href','#/inicio/' );
                    }
                }

                atual.entrar(data.params);
            }
        }

        fusaosite.Navegacao.atual = atual;
        fusaosite.Navegacao.gaPagina( rota, data );
    },

    iniciar: function(){
        //Configurações da navegacao

        var inicial = fusaosite.Navegacao.inicial;
        var rotas = fusaosite.Navegacao.rotas;

        //rotas de navegação
        rotas["home"]       = crossroads.addRoute(inicial + '/:produto:');
        rotas["galeria"]    = crossroads.addRoute('videos');
        rotas["video"]      = crossroads.addRoute('videos/{slug}/:interacao:');

        //adiciona evento para todas as chamadas de rota
        crossroads.routed.add(fusaosite.Navegacao.navega);

        //configurando hasher
        function validaHash(newHash, oldHash){ //metodo chamado a cada troca de hash
            crossroads.parse(newHash);
        }
        function iniciaHash (curHash) {
            if(curHash == ''){
                hasher.replaceHash(inicial);
            };
            crossroads.parse(curHash);
        }

        hasher.initialized.add(iniciaHash);
        hasher.changed.add(validaHash);
        hasher.init();

        fusaosite.Navegacao.tagueamento();
    },

    tagueamento: function() {
        var $head = $('.header.hold');
        var $foot = $('.footer .hold');
        var $home = $('#home.area');
        var $gall = $('#gallery.area');
        var $vid  = $('#video.area');

        var fnEvento = fusaosite.Navegacao.gaEvento;
        // $head.find('.extras .wikiclear').click(function() { fnEvento('menu-superior','clique','wikiclear'); });
        // $foot.find('a.logo').click(function() { fnEvento('rodape','clique','logo-unilever'); });
        // $foot.find('ul li a').click(function(e) {
        //     fnEvento('rodape','clique', $(this).attr('class') );
        // });

        $home.find('.curiosidades a').click(function() { fnEvento('menu-inferior','clique','confira-videos-e-curiosidades'); });
        $home.find('.produtos .link-conheca').click(function() { fnEvento('menu-inferior','clique','conheca-a-linha'); });

        $gall.find('.lista-videos li a').click(function(e) {
            var sl = $(this).attr('href').split('/');
            fnEvento('galeria','clique','video-' +sl[sl.length-1]);
        });

        //o tagueamento do PLAY foi adicionado no config de eventos da Home
    },

    gaPagina: function (rota, data) { //metodo para GA de pagina
        var atual = window.location.href; // rota; // se quiser tracker somente a rota, usar o parametro da função
        var anterior = fusaosite.Navegacao.ultimaHash ;
        if(_gaq && atual != anterior ) {
            // console.log( "GA PageView", rota, data, anterior );
            fusaosite.Navegacao.ultimaHash = atual ;
            _gaq.push(['_trackPageview', atual]);
        }
    },

    gaEvento: function(categoria,acao,titulo) { //metodo para GA de evento (over, click, play, pause, fuck)
        // console.log( "GA Evento", categoria, acao, titulo );
        _gaq.push(['_trackEvent',categoria,acao,titulo]);
    }
};