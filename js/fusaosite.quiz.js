/*===== QUIZ =====*/
fusaosite.Quiz = {
    pontuacao: 0,
    respostas: [],
    configurado: false,
    iniciar: function() {
        var $quiz = $('#quiz');

        if( ! fusaosite.Quiz.configurado ){
            $quiz.find('.perguntas fieldset .slider-respostas').each(function(index, elem) {
                var opcoes = $(this).siblings('.labels');
                var maximo = opcoes.hasClass('four-columns') ? 4 : 2 ;

                var slider = $(this).slider({
                    min   : 1,
                    max   : maximo,
                    range : "min",
                    slide : function( event, ui ) {
                        var position = ui.value - 1;
                        var opcoes = $(ui.handle).parent('.slider-respostas').siblings('.labels');

                        opcoes.find('li').removeClass('active');
                        opcoes.find('li:eq('+position+')').addClass('active');
                    }
                });

                opcoes.find('li').data('slider', slider);
                opcoes.find('li').on('click', function(){
                    var position = $(this).index() + 1;
                    var slider = $(this).data('slider');
                    slider.slider('value', position);

                    $(this).parent('ul').find('li').removeClass('active');
                    $(this).addClass('active');
                });
            });

            $quiz.find('.perguntas .bt-seguinte').click(function(event) {
                event.preventDefault();
                var indice = fusaosite.Quiz.respostas.length ;
                var atual = $quiz.find('.perguntas fieldset').eq(indice);

                var resposta = atual.find('.labels li.active').index();
                fusaosite.Quiz.respostas.push(resposta);

                if( indice == 3 ){
                    $quiz.find('.perguntas').hide();
                    $quiz.find('.infografico').show();
                    fusaosite.Quiz.animarInfografico();
                } else {
                    atual.hide();
                    atual.next('fieldset').fadeIn();

                    $quiz.find('.perguntas .pagination li a').removeClass('active');
                    $quiz.find('.perguntas .pagination li:eq('+ (indice+1) +') a').addClass('active');
                }
            });

            $quiz.find('#step5 .buttons .refazer').click(function(e) {
                e = e || event;
                if(e) e.preventDefault();
                fusaosite.Quiz.iniciar();
            });

            $quiz.find('#step5 .buttons .ver-novamente').click(function(e) {
                e = e || event;
                if(e) e.preventDefault();

                fusaosite.Quiz.animarInfografico();
            });

            fusaosite.Quiz.configurado = true ;
        }

        fusaosite.Quiz.pontuacao = 0 ;
        fusaosite.Quiz.respostas = [];

        $quiz.find('.perguntas .pagination li a').removeClass('active');
        $quiz.find('.perguntas .pagination li:eq(0) a').addClass('active');
        $quiz.find('.infografico').hide();
        $quiz.find('.perguntas').show();
        $quiz.find('.perguntas fieldset').hide();
        $quiz.find('.perguntas fieldset:first').fadeIn();
    },

    animarInfografico: function() {
        var $info = $('#quiz .main.infografico');
        var webkit = /chrome/.test(navigator.userAgent.toLowerCase()) || /safari/.test(navigator.userAgent.toLowerCase()) ;

        function filterUpdate(tw){
            if( webkit ){
                var op = $(tw.target).css('opacity');
                $(tw.target).css('-webkit-filter','grayscale(' + (1-op) + ')')
            }
        }

        var tl = new TimelineMax();

        //step 1
        tl.to( $info.find('#step1').show(), 0.5, { css:{ autoAlpha:1 } } );
        tl.from( $info.find('#step1 .image-1'), 0.8, { css:{ top: '+=550', height: 0 }, ease:Cubic.easeOut } );
        tl.from( $info.find('#step1 .image-1'), 1.2, { css:{ left: '+=453', width: 300 }, ease:Cubic.easeOut }, 0.7 );

        tl.from( $info.find('#step1 .image-4'), 1.2, { css:{ height: 0, width: 0 }, ease:Cubic.easeOut } );

        tl.from( $info.find('#step1 .image-2'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"] } );
        tl.from( $info.find('#step1 .image-3'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], delay:0.5 } );

        tl.to( $info.find('#step1'), 0.5, { css:{ autoAlpha:0 }, delay:5 } );

        // step 2
        tl.to( $info.find('#step2').show(), 0.5, { css:{ autoAlpha:1 } } );
        tl.from( $info.find('#step2 .image-1'), 0.5, { css:{ left:'-=100', opacity:0 }, ease:Quad.easeOut });
        tl.from( $info.find('#step2 .image-2-2'), 0.5, { css:{ scale:0 }, delay:-0.2, ease:Back.easeOut });
        
        tl.from( $info.find('#step2 .image-6'), 0.5, { css:{ scale:0 }, delay:-0.2, ease:Back.easeOut });
        tl.from( $info.find('#step2 .image-3'), 0.5, { css:{ height:0 }, delay:-0.2, ease:Circ.easeInOut });

        tl.from( $info.find('#step2 .image-7'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-14'), 1, { css:{ opacity:0, left:'-=40' }, delay:-0.7, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-15'), 0.5, { css:{ opacity:0, top:'-=40' }, delay:-0.2, ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-10'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-16'), 1, { css:{ opacity:0, left:'+=40' }, delay:-0.4, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });

        tl.from( $info.find('#step2 .image-2'), 0.5, { css:{ scale:0 }, ease:Back.easeOut });
        tl.from( $info.find('#step2 .image-11'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-17'), 1, { css:{ opacity:0, left:'-=40' }, delay:-0.7, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-15-2'), 0.5, { css:{ opacity:0, top:'+=40' }, delay:-0.2, ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-12'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-18'), 1, { css:{ opacity:0, left:'+=40' }, delay:-0.7, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });

        tl.from( $info.find('#step2 .image-4'), 0.5, { css:{ height:0, width:0, left:'+=184', top:'+=167' }, ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-5'), 0.7, { css:{ opacity:0, scale:0.5, left:'-=40', top:'-=40' }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Back.easeOut });
        tl.from( $info.find('#step2 .image-19'), 1, { css:{ opacity:0, left:'-=40' }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });

        tl.from( $info.find('#step2 .image-8'), 0.5, { css:{ height:0, width:0, left:'+=144', top:'+=242' }, ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-13'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step2 .image-20'), 1, { css:{ opacity:0, left:'+=40' }, ease:Circ.easeOut });

        tl.to( $info.find('#step2'), 0.5, { css:{ autoAlpha:0 }, delay:5 } );

        // step 3
        if( fusaosite.Quiz.respostas[0] == 0 ){ // 0 = vaivolta || 1 = cronico
            $info.find('#step3 .cronico').hide();
            $info.find('#step3 .vaivolta').show();
        } else {
            $info.find('#step3 .vaivolta').hide();
            $info.find('#step3 .cronico').show();
        }

        tl.from( $info.find('#step3').show(), 0.5, { css:{ autoAlpha:0 }} );
        tl.from( $info.find('#step3 .image-2'), 1, { css:{ opacity:0, left:'-=40' }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step3 .image-3'), 1, { css:{ opacity:0, left:'-=40' }, delay:-0.5, ease:Circ.easeOut });

        tl.from( $info.find('#step3 .image-1'), 0.7, { css:{ opacity:0, scale:0 }, delay:-0.5, ease:Back.easeOut });
        tl.from( $info.find('#step3 .image-7'), 0.7, { css:{ opacity:0, scale:0 }, delay:-0.5, ease:Back.easeOut });

        tl.from( $info.find('#step3 .image-4'), 1, { css:{ opacity:0, left:'-=40' }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step3 .image-5'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });

        tl.from( $info.find('#step3 .image-6'), 1, { css:{ opacity:0, left:'-=40' }, ease:Circ.easeOut });
        tl.from( $info.find('#step3 .image-8'), 0.5, { css:{ opacity:0 }, delay:-0.4, ease:Circ.easeOut });

        tl.from( $info.find('#step3 .image-9'), 0.8, { css:{ height:0, width:0 }, ease:Circ.easeInOut });
        tl.from( $info.find('#step3 .image-10'), 0.5, { css:{ height:0 }, delay: -0.4, ease:Circ.easeInOut });

        tl.from( $info.find('#step3 .image-11'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step3 .image-12'), 1, { css:{ opacity:0, left:'-=40' }, ease:Circ.easeOut });
        tl.to( $info.find('#step3'), 0.5, { css:{ autoAlpha:0 }, delay:5 } );

        // step 4
        tl.from( $info.find('#step4').show(), 0.5, { css:{ autoAlpha:0 }});

        if ( fusaosite.Quiz.respostas[2] == 0 ) { //0 = semcaspa || 1 = hidratados
            $info.find('#step4 .hidratados').hide();

            tl.from( $info.find('#step4 .image-1'), 0.5, { css:{ opacity:0, left:'-=40' }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-3'), 0.5, { css:{ height:0 }, delay: -0.4, ease:Circ.easeInOut });
            tl.from( $info.find('#step4 .image-2'), 0.5, { css:{ opacity:0, left:'-=40' }, delay:-0.3, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-6'), 0.5, { css:{ opacity:0, left:'-=40' }, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-5'), 0.5, { css:{ height:0 }, delay: -0.2, ease:Circ.easeInOut });
            tl.from( $info.find('#step4 .image-7'), 0.5, { css:{ opacity:0, left:'+=40' }, delay:-0.2, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-9'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-8'), 0.5, { css:{ width:0 }, delay: -0.2, ease:Circ.easeInOut });
            tl.from( $info.find('#step4 .image-10'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        } else {
            $info.find('#step4 .semcaspa').hide();

            tl.from( $info.find('#step4 .image-1'), 0.5, { css:{ opacity:0, left:'-=40' }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-2'), 0.5, { css:{ opacity:0, left:'-=40' }, delay:-0.3, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-8'), 0.5, { css:{ height:0 }, delay:-0.3, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-9'), 0.5, { css:{ height:0 }, delay:-0.3, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-3'), 0.5, { css:{ opacity:0, left:'-=40' }, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-13'), 0.8, { css:{ height:0 }, delay:-0.3, ease:Circ.easeInOut });
            tl.from( $info.find('#step4 .image-4'), 0.5, { css:{ opacity:0, left:'+=40' }, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-10'), 0.8, { css:{ width:0 }, delay:-0.3, ease:Circ.easeInOut });
            tl.from( $info.find('#step4 .image-11'), 0.8, { css:{ width:0 }, delay:-0.3, ease:Circ.easeInOut });
            tl.from( $info.find('#step4 .image-7'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-12'), 0.8, { css:{ height:0, top: '+=179' }, delay:-0.3, ease:Circ.easeInOut });
            tl.from( $info.find('#step4 .image-5'), 0.5, { css:{ opacity:0, left:'-=40' }, ease:Circ.easeOut });
            tl.from( $info.find('#step4 .image-6'), 0.5, { css:{ opacity:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        }

        tl.to( $info.find('#step4'), 0.5, { css:{ autoAlpha:0 }, delay:5 } );

        // step 5
        tl.from( $info.find('#step5').show(), 0.5, { css:{ autoAlpha:0 }} );
        tl.from( $info.find('#step5 .image-1'), 1, { css:{ opacity:0, left:'-=40' }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Circ.easeOut });
        tl.from( $info.find('#step5 .image-2'), 0.5, { css:{ opacity:0, left:'-=40' }, delay:-0.3, ease:Circ.easeOut });
        tl.from( $info.find('#step5 .image-3'), 0.5, { css:{ opacity:0, left:'+=40' }, delay:-0.1, ease:Circ.easeOut });
        tl.from( $info.find('#step5 .image-4'), 0.5, { css:{ opacity:0, left:'-=40' }, delay:-0.1, ease:Circ.easeOut });
        tl.from( $info.find('#step5 .image-5'), 0.5, { css:{ height:0 }, delay:-0.3, ease:Circ.easeOut });
        tl.from( $info.find('#step5 .image-6'), 1, { css:{ opacity:0, scale:0 }, onUpdate:filterUpdate, onUpdateParams:["{self}"], ease:Back.easeOut });
        tl.from( $info.find('#step5 .image-7'), 1.5, { css:{ width:0 }, delay:-0.5, ease:Circ.easeOut });
        tl.staggerFrom( $info.find('#step5 .buttons li a'), 0.5, { css:{ opacity:0, marginLeft:-40 }, delay:0.5, ease:Circ.easeOut });
    }
}