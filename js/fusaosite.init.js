//para remover os debugs, modificar
var fusaosite = window.fusaosite || {};

var debugging = true;
if( ! debugging ){
    console.log = function(args) {};
}

//Objeto que será usado como objeto comum para algumas chamadas de todas as áreas
fusaosite.Area = function(id){
    this.id = id;
    this.aberto = false;
    this.comecouEntrada = new signals.Signal();
    this.terminouEntrada = new signals.Signal();
    this.comecouSaida = new signals.Signal();
    this.terminouSaida = new signals.Signal();
};

fusaosite.Area.prototype.entrar = function(params){
    this.comecouEntrada.dispatch();
};

fusaosite.Area.prototype.sair = function(){
    this.comecouSaida.dispatch();
};