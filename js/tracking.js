(function(window) {

    'use strict';

    var doc = window.document,
        $ = window.jQuery;

    function gaUpdate(options, href, target) {
        // https://developers.google.com/analytics/devguides/collection/gajs/eventTrackerGuide
        // _trackEvent(category, action, opt_label, opt_value, opt_noninteraction)
        var opts = $.extend({}, {
            category: 'site',
            action: 'click',
            label: null,
            value: null
        }, options),
            href = href || null,
            target = target || null;

        window._gaq.push(['_trackEvent', opts.category, opts.action, opts.label, opts.value]);
        console.log("gaUpdate", opts, href, target);

        if (href) {
            window._gaq.push(function() {
                if (target) {
                    window.open(href,target);
                } else {
                    window.location.href = href;
                }
            });
        }
    }

    function cb(ev) {
        var $this = $(this),
            that = ev.data.that,
            info = $this.data(that.dataNome),
            parse = info ? $.parseJSON(String(info).replace(/['"]/g, "\"")) : null,
            href = this.href || null,
            target = this.target || null;

        if (target) ev.preventDefault();

        // console.log(parse, href);
        if (parse) gaUpdate(parse, href, target);
    }

    function Tracking(escopo, seletor, evento, nome) {
        this.escopo = escopo || 'body';
        this.seletor = seletor || null;
        this.evento = evento || 'click.Tracking';
        this.dataNome = nome || 'track';
    }

    Tracking.prototype.on = function() {
        $(this.escopo).on(this.evento, this.seletor, {"that": this}, cb);
    }

    Tracking.prototype.off = function(ev) {
        $(this.escopo).off(this.evento, this.seletor, {"that": this}, cb);
    }

    window.Tracking = Tracking;

})(window);