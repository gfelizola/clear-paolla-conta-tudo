/*===== CARREGAMENTO =====*/
fusaosite.mapa = {};
fusaosite.Carregamento = {
    loading: null,
    spinner: null,
    carregador: null,
    //arquivos que ficam carregados e em cache
    arquivos: [
        'img/logo-clear-preto.png',
        'img/logo-clear.png',
        'img/logo-unilever.png',
        'img/pattern.png',
        'img/home/home-fundo.jpg',
        'img/home/home-thumb-galeria.jpg',
        'img/home/frame1-1.png',
        'img/home/frame1-2.png',
        'img/home/frame2-1.png',
        'img/home/frame2-2.png',
        'img/home/frame3-1.png',
        'img/home/frame3-2.png',
        'img/home/tit-alisamento-combina-com-anticaspa.png',
        'img/home/tit-cabelos-sem-caspa-e-hidratados-e-possivel.png',
        'img/home/tit-posso-usar-meu-anticaspa-todos-os-dias.png',
        'img/produtos/home-produtos-folha-frente.png',
        'img/produtos/home-produtos-folha-trans.png',
        'img/produtos/produto-cond-bege.png',
        'img/produtos/produto-cond-branco.png',
        'img/produtos/produto-xampu-bege.png',
        'img/produtos/produto-xampu-branco.png',
        'img/produtos/produtos-aberto-bg.png',
        'img/produtos/produtos-bg.png',
        'img/galeria/galeria-bg-externo.png',
        'img/galeria/galeria-bg-interno.jpg',
        'img/galeria/galeria_video01.jpg',
        'img/galeria/galeria_video02.jpg',
        'img/galeria/galeria_video03.jpg'
    ],

    parar: function(){
        //caso exista algum carregamento, parar
        if( fusaosite.Carregamento.carregador != null ) fusaosite.Carregamento.carregador.close();
    },
    carregouArquivo: function(ev){
        //adiciona cada imagem carregada em um mapa (ainda não está sendo ultilizado, e se não precisa, é bom remover para ganhar performance)
        var img = ev.result;
        fusaosite.mapa[ev.src] = img;
    },
    completou: function(ev) {
        //Terminou de carregar tudo
        fusaosite.Carregamento.esconder();
        fusaosite.Navegacao.iniciar();
    },
    progresso: function (ev) {
        // nome explica tudo
        // console.log( "Carregamento progresso", ev.progress );
    },
    erro: function(ev){

    },
    iniciar: function () {
        //Se ainda não foi criado o spinner de loading, criar
        if( fusaosite.Carregamento.loading == null ){
            fusaosite.Carregamento.loading = $('.loading');

            fusaosite.Carregamento.spinner = new Spinner({
                lines    : 10,
                length   : 15,
                width    : 3,
                radius   : 6,
                corners  : 1,
                speed    : 2,
                trail    : 50,

                rotate   : 0,
                direction: 1, // 1: clockwise, -1: counterclockwise
                color    : '#80be00', // #rgb or #rrggbb
                shadow   : false, // Whether to render a shadow
                hwaccel  : false, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex   : 99, // The z-index (defaults to 2000000000)
                top      : '6px', // Top position relative to parent in px
                left     : 'auto' // Left position relative to parent in px
            }).spin(fusaosite.Carregamento.loading.find('.spin').get(0));
        }

        fusaosite.Carregamento.parar();

        //configura fila e eventos
        var fila = new createjs.LoadQueue(true);
        fila.addEventListener("fileload", fusaosite.Carregamento.carregouArquivo);
        fila.addEventListener("complete", fusaosite.Carregamento.completou);
        fila.addEventListener("progress", fusaosite.Carregamento.progresso);
        fila.addEventListener("error", fusaosite.Carregamento.erro);
        fila.loadManifest(fusaosite.Carregamento.arquivos, false);

        fusaosite.Carregamento.carregador = fila;
        fusaosite.Carregamento.mostrar(); //mostrar spinner de loading
        fila.load();
    },
    mostrar: function () {
        if( fusaosite.Carregamento.loading.find('.spinner').length <= 0 ) fusaosite.Carregamento.spinner.spin(); //só chamadr esse metodo se o spinner tiver sido desligado
        fusaosite.Carregamento.loading.stop().fadeIn(400);
    },
    esconder: function() {
        fusaosite.Carregamento.loading.stop().fadeOut(400, function() {
            fusaosite.Carregamento.spinner.stop();
        });
    },
    animar: function(p) {
        //TODO
    }
};