var ytplayer,
    playerCarregado = false,
    videoIdAtual = '';

// This function is called when an error is thrown by the player
var onPlayerError = function(errorCode){
    console.log('An error occured of type:' + errorCode);
};

var onPlayerStateChange = function(newState){
    console.log('playerState', newState);
    if( newState == 1 ){ //playing
        $('#video .jp-controls .jp-play').hide();
        $('#video .jp-controls .jp-pause').show().click(function(e) {
            e = e || event; if(e) e.preventDefault();
            pauseVideoYT();
        });
        window.progressTween.resume();

    } else  if( newState == 0 ){ //ended
        $('.header').removeClass('interna');
        fusaosite.Video.mostrarConteudoFinal();
        window.progressTween.pause();

    } else {
        $('#video .jp-controls .jp-pause').hide();
        $('#video .jp-controls .jp-play').show().click(function(e) {
            e = e || event; if(e) e.preventDefault();
            playVideoYT();
        });

        window.progressTween.pause();
    }
};

var playVideoYT = function(){
    if (ytplayer) {
        ytplayer.playVideo();
    }
};

var pauseVideoYT = function(){
    if (ytplayer) {
        ytplayer.pauseVideo();
    }
};

function updatePlayerInfo() {
    if(ytplayer && ytplayer.getDuration) {
        var tempoTotal = ytplayer.getDuration();
        var tempoAtual = ytplayer.getCurrentTime();

        $('#video .jp-controls .currentTime .jp-current-time').html( formataTempo( tempoAtual ) );
        $('#video .jp-controls .currentTime .jp-duration').html( formataTempo( tempoTotal ) );

        $('#video .jp-controls .timeline .seekPercent').css('width', (ytplayer.getVideoLoadedFraction() * 100) + '%' )

        var pTocou = Math.ceil((tempoAtual * 100) / tempoTotal);
        $('#video .jp-controls .timeline .playhead').css('width', pTocou + '%' )
    }
}

var swapYoutubeVideo = function(videoId){
    videoIdAtual = videoId;
    if( ! playerCarregado || ytplayer === null ){
        loadPlayer();
    } else {
        ytplayer.loadVideoById(videoIdAtual);

        setInterval(updatePlayerInfo, 250);
        updatePlayerInfo();
    }
};

var onYouTubePlayerReady = function(playerId){
    ytplayer = document.getElementById('yTplayer');
    playerCarregado = true;
    
    ytplayer.addEventListener('onStateChange', 'onPlayerStateChange');
    ytplayer.addEventListener('onError', 'onPlayerError');
    swapYoutubeVideo(videoIdAtual);

    $('.header').addClass('interna');
};

var loadPlayer = function(){
    var params = { allowScriptAccess: 'always' };
    var atts = { id: 'yTplayer' };
    swfobject.embedSWF('http://www.youtube.com/apiplayer?version=3&modestbranding=1&enablejsapi=1&playerapiid=yTplayer', 
        'yTplayer', '100%', '100%', '9', null, null, params, atts);
};

var formataTempo = function(tempo) {
    var hours = Math.floor(tempo / (60 * 60));
   
    var divisor_for_minutes = tempo % (60 * 60);
    var minutes = Math.floor(divisor_for_minutes / 60);
 
    var divisor_for_seconds = divisor_for_minutes % 60;
    var seconds = Math.ceil(divisor_for_seconds);
   
    var tempo = {
        "h": hours,
        "m": minutes,
        "s": seconds
    };

    var minutos = tempo.m + '';
    var segundos = tempo.s + '';

    if( minutos.length < 2 ) minutos = '0' + minutos;
    if( segundos.length < 2 ) segundos = '0' + segundos;

    return minutos + ':' + segundos;
}