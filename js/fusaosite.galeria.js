/*===== GALERIA =====*/
fusaosite.Galeria = {
    area: new fusaosite.Area('galeria'),
    entrar: function(params) {
        if( ! fusaosite.Home.area.aberto && ! params.abrirImediatamente ){
            fusaosite.Home.area.terminouEntrada.add( fusaosite.Galeria.entrar );
            fusaosite.Home.entrar();
        } else {
            fusaosite.Galeria.animarEntrada();
        }
    },
    sair: function(params) {
        var $gal = $('#gallery.area');
        var tl = new TimelineMax({onComplete: function() {
            $gal.hide();
        }});
        tl.to( $gal, 1, { css:{ opacity:0 }});

        $gal.find('.tabs-title').off('click.trocaTabs');

        fusaosite.Galeria.area.terminouSaida.dispatch(params);
    },
    animarEntrada: function(params) {
        var $gal = $('#gallery.area');
        var tl = new TimelineMax({
            onComplete: fusaosite.Galeria.area.terminouEntrada.dispatch,
            onCompleteParams: [params]
        });

        $gal.show();
        tl.fromTo( $gal, 1, { css:{ opacity:0 }}, { css:{ opacity:1 }, ease:Bounce.easeOut });
        tl.from( $gal.find('.main'), 0.7, { css:{ width:'0%', marginLeft:'50%' }, ease:Cubic.easeOut });
        tl.from( $gal.find('.main'), 0.7, { css:{ height:1, marginTop: 250 }, ease:Cubic.easeOut });
        tl.from( $gal.find('.tabs-title'), 0.4, { css:{ height:0, marginTop: '+=34', paddingTop:0, paddingBottom:0 }, ease:Cubic.easeOut });
        tl.staggerFrom( $gal.find('.main ul li'), 0.4, { css:{ opacity:0 }});
        tl.from( $gal.find('.geral-fechar'), 0.4, { css:{ opacity:0, scale:1.5 }, delay:0.5});

        $gal.find('.main ul li a').hover(function() {
            TweenMax.to( $(this).find('img'), 0.5, { css:{ opacity:0.5 } } );
            TweenMax.to( $(this).find('span'), 0.5, { css:{ opacity:1 } } );
        }, function() {
            TweenMax.to( $(this).find('img'), 0.5, { css:{ opacity:1 } } );
            TweenMax.to( $(this).find('span'), 0.5, { css:{ opacity:0 } } );
        });

        $gal.find('.tabs-title').on('click.trocaTabs', function () {
            $(this).parent().find('.tabs-title').removeClass('active');
            $(this).addClass('active');
            fusaosite.Galeria.trocaConteudo($(this).attr('data-content'));
        });
    },
    trocaConteudo: function (selector) {

        var $gal = $('#gallery.area');

        $gal.find('.hold').not(selector).stop().fadeOut(300, function () {
            $(selector).stop().fadeIn();
        });

        // Executa animação de troca de tabs
    }
}