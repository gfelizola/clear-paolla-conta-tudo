//====================
// Cover effect
// Author: Thiago Lagden
// deps: Jquery
//====================

/**
 * @constructor
 */
function Cover(vp, els) {
    var that = this;
    this.$win = $(window);
    this.viewport = vp;
    this.els = [];
    $.each(els, function(i, v){
        that.els.push($(v));
    });

    // Tamanho Original
    this.ow = this.els[0].width();
    this.oh = this.els[0].height();

    // Init
    this.init();
}

Cover.prototype.init = function() {
    this.$win
        .on('resize.cover', {"that": this}, this.resize)
        .trigger('resize.cover');
};

Cover.prototype.trigger = function() {
    this.$win.trigger('resize.cover');
};

Cover.prototype.resize = function(ev) {
    var min_w = 320;
    var that = ev.data.that;
    var $el = that.viewport.parent(),
    $elW = $el.width(),
    $elH = $el.height(),
    ow = that.ow,
    oh = that.oh,
    scale_h = $elW / ow,
    scale_v = $elH / oh,
    scale = scale_h > scale_v ? scale_h : scale_v;

    that.viewport.width($elW);
    that.viewport.height($elH);

    if (scale * ow < min_w) scale = min_w / ow;

    $.each(that.els, function(i, $v){
        $v.width(scale * ow);
        $v.height(scale * oh);
    });

    that.viewport.scrollLeft((that.els[0].width() - $el.width()) / 2);
    that.viewport.scrollTop((that.els[0].height() - $el.height()) / 2);
};