/*===== VIDEO =====*/
fusaosite.Video = {
    area: new fusaosite.Area('video'),
    videoAtual: 1,
    enderecos: [],
    mostrando: null,
    YTinstancia: null,
    entrar: function(params) {
        var interacao,
            $vid = $('#video.area');

        $('.footer, .header').addClass('interna');

        if( fusaosite.Video.enderecos.length <= 0 ){
            fusaosite.Video.configurarArea();
        }

        if( params != null ){
            var sub = params[0];
            if( typeof(sub) === "string" && isNaN(sub) ){
                sub = Number( sub.substring( sub.length - 1 ) );
                var indiceVideo = Number(sub) - 1 ;
            }

            if ( params.length > 1 ) interacao = params[1];
        }

        if( interacao === undefined ){
            fusaosite.Video.Interacao.fecharInteracao();
        }

        if( fusaosite.Video.area.aberto ){
            if( indiceVideo != fusaosite.Video.videoAtual ){
                fusaosite.Video.trocarVideo(indiceVideo);
            } else {
                if( interacao ){
                    fusaosite.Video.Interacao.mostrarInteracao(interacao);
                } else {
                    fusaosite.Video.YTinstancia.ref.playVideo();
                }
                
            }
        } else {
            $vid.show();
            var tl = new TimelineMax({
                onComplete: function() {
                    fusaosite.Video.area.aberto = true ;
                    fusaosite.Video.area.terminouEntrada.dispatch(params);

                    TweenMax.delayedCall(1,function() {
                        fusaosite.Video.trocarVideo(indiceVideo);

                        if( interacao ){
                            fusaosite.Video.Interacao.mostrarInteracao(interacao);
                        }
                    });
                }
            });
            tl.fromTo( $vid, 1, { css:{ opacity:0 }}, { css:{ opacity:1 }});
        }

        $vid.find('.tooltip').fadeIn(function () {

            var $tooltip = $(this),
                $btnFechar = $tooltip.find('.fechar'),
                time = setTimeout(function () {
                    $btnFechar.trigger('click');
                }, 6000);

            $btnFechar.on('click', function (e) {
                e.preventDefault();
                $tooltip.fadeOut();
                clearInterval(time);
            });
        });
    },

    sair: function(params) {
        ytmanager.stopTodos();
        var $vid = $('#video.area');

        var tl = new TimelineMax({onComplete: function() {
            $vid.hide();
            fusaosite.Video.area.aberto = false ;
            fusaosite.Video.area.terminouSaida.dispatch(params);
        }});

        tl.to( $vid, 1, { css:{ opacity:0 }});
        $('.footer, .header').removeClass('interna');
    },

    configurarArea: function() {
        for (var i = 0; i < window.videos.length; i++) {
            fusaosite.Video.enderecos.push( window.videos[i].slug )
        };

        $('#video .link-esquerda').data('opacidade-inicial', $('#video .link-esquerda').css('opacity'));
        $('#video .link-direita').data('opacidade-inicial', $('#video .link-direita').css('opacity'));

        $('#video .link-esquerda, #video .link-direita').hover(function() {
            TweenMax.killTweensOf( $(this) );
            TweenMax.to( $(this), 0.5, { css:{ opacity:1 }, ease:Cubic.easeOut });
        }, function() {
            TweenMax.killTweensOf( $(this) );
            TweenMax.to( $(this), 0.5, { css:{ opacity:$(this).data('opacidade-inicial') }, ease:Cubic.easeInOut });
        });

        $('a.botao-wikiclear').click(function(ev) {
            ev.preventDefault();
            window.open( $(this).attr('href') );
        });

        //Manter controles visiveis
        function validaPosicaoSeeker () {
            var $vid = $('#video.area');
            var $wid = $(window);
            var posTotal = $wid.height() + $wid.scrollTop();

            if( $vid.height() > posTotal ){
                $vid.find('.controls').css({top:posTotal - 55});
            } else {
                $vid.find('.controls').css({top:$vid.height() - 55});
            }
        }

        $(window).scroll(validaPosicaoSeeker);
        $(window).resize(validaPosicaoSeeker);
        validaPosicaoSeeker();

        //cria instancia de video do Youtube
        fusaosite.Video.YTinstancia = ytmanager.carregaPlayer('videoPlayerPrincipal',{
            cover: true,
            autoplay: false,
            autoControls: '#video'
        });

        var yti               = fusaosite.Video.YTinstancia;
        var tempoMostrandoCue = 5 ;

        yti.onUpdate = function(e) {
            if( ! fusaosite.Video.Interacao.posicionouPontos ) fusaosite.Video.Interacao.posicionaPontos(e.total);
            fusaosite.Video.Interacao.mostraPontos(e.current);
        };

        yti.onEnded = function(e) {
            // console.log( "Video Ended", e );
            $('.header').removeClass('interna');
            // fusaosite.Video.mostrarConteudoFinal();
            hasher.setHash('inicio');
        };
    },

    trocarVideo: function(qual) {
        if( qual >= 0 ){
            fusaosite.Video.videoAtual = qual;
            fusaosite.Video.removeConteudoFinal();

            var yti             = fusaosite.Video.YTinstancia;
            var currentMedia    = window.videos[qual];

            //troca video do youtube
            ytmanager.carregarVideo('videoPlayerPrincipal', currentMedia.youtubeID);

            var objVideo1, objVideo2;
            switch(qual){
                case 0:
                    objVideo1 = window.videos[2];
                    objVideo2 = window.videos[1];
                    break;
                case 1:
                    objVideo1 = window.videos[0];
                    objVideo2 = window.videos[2];
                    break;
                case 2:
                    objVideo1 = window.videos[1];
                    objVideo2 = window.videos[0];
                    break;
                default:
                    objVideo1 = window.videos[1];
                    objVideo2 = window.videos[0];
                    break;
            }

            //troca links e referencias para outros videos
            $('#video .link-esquerda').attr('href','#/videos/' + objVideo1.slug).text( objVideo1.titulo );
            $('#video .link-direita').attr('href','#/videos/' + objVideo2.slug).text( objVideo2.titulo );

            $('#video .final-opcoes .link-1')
                .attr('href', '#/videos/' + objVideo1.slug )
                .find('img')
                    .attr('src', $('#gallery .lista-videos a[href*="' + objVideo1.slug + '"] img').attr('src') )
                .end().find('h4')
                    .text(objVideo1.titulo);

            $('#video .final-opcoes .link-2')
                .attr('href', '#/videos/' + objVideo2.slug )
                .find('img')
                    .attr('src', $('#gallery .lista-videos a[href*="' + objVideo2.slug + '"] img').attr('src') )
                .end().find('h4')
                    .text(objVideo2.titulo);

            //remove pontos de interacao antigos e cria os novos
            if( yti.pronto ){
                fusaosite.Video.Interacao.criarPontos();
            } else {
                yti.onReady = function(e) {
                    fusaosite.Video.Interacao.criarPontos();
                }
            }
        }
    },

    mostrarConteudoFinal: function() {
        var $vid      = $('#video');

        var tl = new TimelineMax();
        tl.from( $vid.find('.final-opcoes').show(), 0.5, { css:{ opacity:0 } } );
        tl.to( $vid.find('.link-esquerda'), 0.5, { css:{ left:-250 }, ease:Power2.easeIn } );
        tl.to( $vid.find('.link-direita'), 0.5, { css:{ right:-250 }, delay:-0.3, ease:Power2.easeIn } );
        tl.staggerFrom( $vid.find('.final-opcoes .thumbnail'), 0.5, { css:{ top:'-=150', opacity:0 }, ease:Cubic.easeOut }, 0.1 );
        tl.staggerFrom( $vid.find('.final-opcoes .thumbnail h4'), 0.5, { css:{ marginTop:'+=50', opacity:0 }, ease:Cubic.easeOut }, 0.1 );

        fusaosite.Video.chamarAtencaoParaNavegacao();
    },

    removeConteudoFinal: function() {
        $('#video .link-esquerda').css({left:0});
        $('#video .link-direita').css({right:0});
        $('#video .final-opcoes').hide();
    },

    chamarAtencaoParaNavegacao: function() {
        TweenMax.staggerTo( $('#video .bt-interacao .efeito'), 1.5, { rotation:300, ease:Power2.easeInOut }, 0.5 );
    },

    Interacao: {
        classe: 'bt-interacao',
        posicionouPontos: false,
        tempoMostrandoPonto: 5,
        criarPontos: function(){
            var yti = fusaosite.Video.YTinstancia;
            var currentMedia    = window.videos[fusaosite.Video.videoAtual];
            var pontos = currentMedia.pontos ;

            fusaosite.Video.Interacao.posicionouPontos = false ;
            $(yti.controles).find('.' + fusaosite.Video.Interacao.classe).remove();

            $.each(pontos, function(index, val) {
                var link = $('<a href="#" class="' + fusaosite.Video.Interacao.classe + '" data-track="{\'category\':\'video' + fusaosite.Video.videoAtual + '\', \'action\':\'clique\', \'label\':\'interacao' + (index+1) + '\'}">' +
                    '<h3 class="titulo">' + val.titulo + '</h3>' +
                    '<span class="efeito"></span>' +
                    '<span class="imagem"></span>' +
                    '<span class="icone"></span>' +
                    '<img src="img/interacao/botao-ponto.jpg" alt="" class="cue-point" />' +
                    '</a>');

                var urlFinal = val.slug; //.replace('{current-media-slug}', currentMedia.slug);
                link.attr('href','#/videos/' + currentMedia.slug + '/' + urlFinal);
                link.addClass(val.slug);
                link.data('posicao', val.cue);

                $(yti.controles).find('.timeline').append(link);
            });
        },
        posicionaPontos: function(total) {
            if( ! fusaosite.Video.Interacao.posicionouPontos ){
                $(fusaosite.Video.YTinstancia.controles).find('.' + fusaosite.Video.Interacao.classe).each(function(ind, el) {
                    $(this).css({left: Math.round( ( 760 * $(this).data('posicao') ) / total ) - 100 }).show();
                });
                fusaosite.Video.Interacao.posicionouPontos = true ;
            }
        },
        mostraPontos: function(tempo) {

            var tempoMostrandoCue = fusaosite.Video.Interacao.tempoMostrandoPonto;
            var controles = $(fusaosite.Video.YTinstancia.controles);

            controles.find('.' + fusaosite.Video.Interacao.classe).each(function(index) {
                var posicao = $(this).data('posicao');
                if( tempo >= posicao && tempo < posicao + tempoMostrandoCue ){
                    if( $(this).css('opacity') == 0 ){
                        TweenMax.fromTo( $(this), 0.3, { css:{autoAlpha:0, scale:2}}, { css:{autoAlpha:1, scale:1, transformOrigin:'100px 100px'}, ease:Back.easeOut });
                        TweenMax.from( $(this).find('.cue-point'), 0.5, { css:{height:0}, ease: Back.easeOut, delay: 0.3 });
                        TweenMax.from( $(this).find('.icone'), 0.5, { css:{ opacity:0 }, ease: Back.easeOut, delay:0.7 });
                        TweenMax.from( $(this).find('.titulo'), 0.5, { css:{ left:0, opacity:0 }, ease: Power2.easeOut, delay:0.7 });

                        TweenMax.to( $(this).find('.efeito'), 2.5, { directionalRotation:"120_cw", ease: Back.easeOut, delay:1 });
                    }
                } else if( tempo > posicao + tempoMostrandoCue ){
                    if( ! $(this).hasClass('saido') ){
                        var posIcone = $(this).position();
                        var posTime = controles.find('.timeline').offset();

                        $(this).css({left: posTime.left + posIcone.left, top: posIcone.top + 7 }).addClass('saido');
                        controles.append( $(this) );

                        TweenMax.to( 
                            $(this), 1, 
                            { css:{ left:-30, top: getPosicaoTop($(this).index()), scale:0.4, autoAlpha:1 }, ease:Strong.easeInOut } 
                        );
                        TweenMax.to( $(this).find('.efeito'), 1, {rotation:0, ease: Strong.easeOut });
                    }
                } else {
                    if( $(this).css('opacity') > 0 && ! $(this).hasClass('saido') ){
                        TweenMax.to( $(this), 0.3, { css:{autoAlpha:0} });
                    }
                }
            });
        },
        mostrarInteracao: function(id){
            ytmanager.pausaTodos();

            var $interacao = $('#' + id);
            var tl = new TimelineMax({
                onComplete: fusaosite.Video.Interacao.iniciaInteracao,
                onCompleteParams: [id]
            });

            $interacao.show();
            tl.fromTo( $interacao, 1, { css:{ opacity:0 }}, { css:{ opacity:1 }, ease:Bounce.easeOut });
            tl.from( $interacao.find('.main'), 0.7, { css:{ width:0, marginLeft: 0 }, ease:Cubic.easeOut });
            tl.from( $interacao.find('.main'), 0.7, { css:{ height:1, marginTop: 0 }, ease:Cubic.easeOut });

            $('#' + id + ' .geral-fechar').attr('href','#/videos/' + window.videos[ fusaosite.Video.videoAtual ].slug );
            fusaosite.Video.mostrando = $interacao.find('.jp-player') ;
        },

        iniciaInteracao: function  (id) {
            var $interacao = $('#' + id);
            if( $interacao.hasClass('interacao-video') ){
                var pontos = window.videos[ fusaosite.Video.videoAtual ].pontos
                for (var i = 0; i < pontos.length; i++) {
                    var p = pontos[i];
                    if( p.slug == id ){
                        var medias = p.video;
                        var idPlayer = id.split('-').join('');

                        //cria instancia de video do Youtube
                        var yti = ytmanager.carregaPlayer('videoPlayer' + idPlayer,{
                            autoplay: p.youtubeID,
                            autoControls: '#' + id + ' .container-player'
                        });

                        if( id != 'wikiclear' ){
                            yti.onEnded = function(e) {
                                hasher.setHash('videos/' + window.videos[ fusaosite.Video.videoAtual ].slug );
                            };
                        }
                    }
                };
            } else if( $interacao.hasClass('quiz') ){
                fusaosite.Quiz.iniciar();
            }
        },

        fecharInteracao: function(){
            $('.area.interacao').stop().fadeOut();
            if( fusaosite.Video.mostrando != null ){
                fusaosite.Video.mostrando.jPlayer('pause');
                fusaosite.Video.mostrando.jPlayer('clearMedia');
            }
            fusaosite.Video.mostrando = null ;
        }
    }
}